const util = require('util');
const fs = require('fs');
const path = require('path');
const axios = require('axios');

(async () => {
	try {
		const imageContent = await util.promisify(fs.readFile)(path.join(__dirname, '../images/realTest.jpg'), 'base64');

		const { data = {} } = await axios({
			method: 'post',
			url: `http://207.154.215.144:3009/api/users/test-tesseract-ocr`,
			data: {
				fileBase64content: imageContent,
				extension: 'jpg'
			},
		});

		console.log('response', data)
	} catch (ex) {
		console.error(ex)
	}
})();