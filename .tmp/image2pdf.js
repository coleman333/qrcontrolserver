const path=require('path');
const tesseract = require('node-tesseract');

	const options = {
		// l: 'eng+rus+ukr',
		l: 'ukr',
		// l: 'rus',
		psm: 6,
		// binary: '/usr/local/bin/tesseract'
		binary: '/usr/bin/tesseract'
	};

	tesseract.process(path.join(__dirname,'../images/realTest.jpg'), options, function(err, text) {
		if(err) {
			console.error(err);
		} else {
			console.log(text);
		}
	});
