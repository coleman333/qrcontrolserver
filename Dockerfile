FROM node:8-alpine
EXPOSE 3003 4433

RUN apk update
RUN apk add --no-cache tesseract-ocr
RUN apk add --no-cache tesseract-ocr-dev
RUN apk add --no-cache tesseract-ocr-data-rus tesseract-ocr-data-ukr
RUN apk add --no-cache python gcc

WORKDIR /qrcontrolserver

COPY ./package*.json ./
RUN npm i

COPY . .

RUN mkdir images \
    && npm run build:dev \
    && chown -R 1000:1000 .

CMD ["npm", "run", "dev"]