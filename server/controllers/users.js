const axios = require("axios");

const {TESSDATA_PREFIX} = (~/Downloads/);
const {Expo} = require('expo-server-sdk');

const passport = require('passport');
const path = require('path');
const multer = require('multer');
let bcrypt = require('bcrypt');
let SALT_WORK_FACTOR = 10;
let userModel = require('../../database/models/userModel');
let logOfVisiting = require('../../database/models/logOfVisiting');
let productModel = require('../../database/models/product');
let scanOfDocModel = require('../../database/models/scanOfDoc');
let preEngraving = require('../../database/models/preEngraving');
let tesseract = require('node-tesseract');
const _ = require('lodash');
const moment = require('moment');
const base64ToImage = require('base64-to-image');
// const testImg = '../../images/testText.jpg';
// const testImg = '../../images/1231.png';
// const testImg = '../../images/realTest.jpg';
// const testImg = '../../images/realPhoto.jpg';
// const testImg = '../../images/realPhoto2.jpg';
const testImg = '../../images/1.3.jpg';

const fs = require('fs');
const sharp = require('sharp');

module.exports.upload = multer({

    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            const absolutePath = path.join(__dirname, '../../images/');
            cb(null, absolutePath);
            console.log('this is multer');
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    })
});


// module.exports.registration = function (req, res, next) {
//     // console.dir(JSON.parse(req.body.user));
//     //  req.body.user=JSON.parse(req.body);
//     console.dir(req.body);
//     // console.dir(req.file);
//     let user = req.body;
//
//     try {
//         user.avatar = `/images/${req.file.filename}`; //'images'+req.file.filename
//     } catch (ex) {
//     }
//     userModel.create(user, function (err, user) {
//         if (err) {
//             console.error(err);
//             return next(err);
//         }
//         req.logIn(user, function (err) {
//             if (err) {
//                 console.error(err);
//                 return next(err);
//             }
//             res.json(_.omit(user.toJSON(), 'password'));
//         });
//         // login2(req.body.user);
//         // res.json(_.omit(user.toJSON(), 'password'));
//     });
//
// };

module.exports.registration = function (req, res, next) {
    console.log(req.body);

    // console.log(req.body);
    const user = req.body;
    userModel.create(user, function (err, user) {
        if (err) {
            console.error(err);
            return next(err);
        }
        console.log('this is server side', user);
        res.json(user);
    })
}

function encryptPassword(password, callbackEncryptPassword) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return callbackEncryptPassword(err);

        // hash the password along with our new salt
        bcrypt.hash(password, salt, function (err, hash) {
            if (err) return callbackEncryptPassword(err);
            // override the cleartext password with the hashed one
            callbackEncryptPassword(null, hash);
        });
    });
}

//
// module.exports.login = function (req, res, next) {
//     // encryptPassword(user.password, function (err, encryptedPassword) {
//     //     if (err) {
//     //         console.error(err);
//     //         return next(err);
//     //     }
//     //     user.password = encryptedPassword;
//
//
//     passport.authenticate('local', function done(err, user, info) {
//         // let userModel ={};
//         // userModel.find({category: req.params.email},function (err,user) {
//         //
//         // });
//         if (err) {
//             console.error(err);
//             return next(err);
//         }
//         if (user) {
//             req.logIn(user, function (err) {
//                 if (err) {
//                     console.error(err);
//                     return next(err);
//                 }
//                 res.json({
//                     _id: user._id.toString(),
//                     firstName: user.firstName,
//                     avatar: user.avatar
//                     // message: 'You have access!'
//                 });
//             });
//         } else {
//             res.statusCode = 403;
//             res.json({
//                 message: info.message
//             });
//         }
//     })(req, res, next);
//     // })
// };

// module.exports.testTesseractRegex =  function(req,res,next){
//     // let fileJpg = '';
//     try {
//         // const {fileBase64content, extension} = req.body;
//         // saves base64 content to the file.
//         // let pipeline ='';
//         // let pathToFile = path.join(__dirname, `../../images/image-${Date.now().valueOf()}.${extension}`);
//         // if you use callback, nothing will return.  I need this file to tesseract. It will be available through other way
//         // fs.writeFileSync(pathToFile, fileBase64content, {encoding: 'base64'})//, function (err) {
//
//         // sharp(pathToFile).rotate(90)
//
//         const options = {
//             // l: 'eng+rus+ukr',
//             l: 'ukr',
//             // l: 'rus',
//             psm: 6,
//             // binary: '/usr/local/bin/tesseract'
//             binary: '/usr/bin/tesseract'
//         };

//         tesseract.process(testImg, options, async function (err, text) {
//             if (err) {
//                 console.error(err);
//             } else {
//                 console.log(text);
//
//                 // axios({
//                 // 	method: 'post',
//                 // 	url: `http://localhost:3003/api/users/test-tesseract-ocr`,
//                 // 	data: {text}
//                 // })
//                 // 	.then(() => {
//
//                 let companyNameREGEX='^АО&#171&#187$';
//                 let companyName =text.match(companyNameREGEX);
//                 console.log(companyName);
//                 res.json(text)
//                 //
//                 // 	})
//                 // 	.catch(next);
//
//                 // try {
//                 // 	const result = await axios({
//                 // 		method: 'post',
//                 // 		url: `http://localhost:3003/api/users/test-tesseract-ocr`,
//                 // 		data: {text}
//                 // 	})
//                 //
//                 // 	res.json()
//                 // }catch (ex){
//                 // 	next(ex)
//                 // }
//             }
//         });
//
//     } catch (ex) {
//         next(ex)
//     }
// }

module.exports.testTesseract = async function (req, res, next) {
    try {
        const {fileBase64content, extension} = req.body;
        // saves base64 content to the file.
        // let pipeline ='';
        let pathToFile = path.join(__dirname, `../../images/image-${Date.now().valueOf()}.${extension}`);
        // if you use callback, nothing will return.  I need this file to tesseract. It will be available through other way
        fs.writeFileSync(pathToFile, fileBase64content, {encoding: 'base64'}) //, function (err) {

        await sharp(pathToFile).rotate(90);

        const options = {
            // l: 'eng+rus+ukr',
            l: 'ukr',
            // l: 'rus',
            psm: 3,
            // binary: '/usr/local/bin/tesseract'
            binary: '/usr/bin/tesseract'
        };

        const text = await require('util').promisify(tesseract.process).call(tesseract, pathToFile, options);


        // let spaceLessTextREGEX =
        const newText2 = text.replace(/^\s*$(?:\r\n?|\n)/gm, '');
        const newText = newText2.replace(' ', '');

        let companyNameREGEX = /[«<](.*)[>»]/;
        // let companyNameREGEX = new RegExp(`[«<](.*)[>»]`);
        let companyName = newText.match(companyNameREGEX);
        // console.log(companyName[1]);

        let formNumberREGEX = /№(.*)\n/i;
        let formNumber = newText.match(formNumberREGEX);
        // console.log(formNumber);

        // let IncomeOrderREGEX = /.*ОРДЕР[\D\s]*(\d*)[\D\s\n]*Номер.*/m;
        // match(/.*ОРДЕР[\D\s]*(\d*)[\D\s\n]*Номер.*/m)[1]
        let incomeOrder = '';
        if(newText.match(/.*ОРДЕР[\D\s]*(\d*)[\D\s\n]*Номер.*/m)!==null) {
            incomeOrder = _.get(newText.match(/.*ОРДЕР[\D\s]*(\d*)[\D\s\n]*Номер.*/m),1,0);
            // console.log(IncomeOrder);
        }
        // else{incomeOrder = 0}
        // let providerCode = '';
        // if(newText.match(/.*АЗ\/ч[\D\s]*(\d*)[\D\s\n]*№.*/m)!==null) {
        //     providerCode = newText.match(/.*АЗ\/ч[\D\s]*(\d*)[\D\s\n]*№.*/m)[1];
        // }
        // else{providerCode = 0}

        console.log('----------');
        console.log(newText.match(/\d{4,}/mg));
        console.log('----------');
        let providerCode = '';
        if(newText.match(/\d{4,}/mg)[4]!==null){
            providerCode = _.get(newText.match(/\d{4,}/mg),4,0);
            // console.log('code--',newText.match(/\d{4,}/mg)[2]);
        }
        console.log('----------');

        let guestNumber ='';
        if(newText.match(/\d{4,}/mg)[5]!==null){
            guestNumber = _.get(newText.match(/\d{4,}/mg),5,0);
        // console.log('code--',newText.match(/\d{4,}/mg)[5]);
        }
        console.log('----------');
        let buyingOrder ='';
        if(newText.match(/\d{4,}/mg)[7]!==null){
            buyingOrder = _.get(newText.match(/\d{4,}/mg),7,0);
            // console.log('code--',newText.match(/\d{4,}/mg)[7]);
        }
        console.log('----------');


        console.log(newText , '\n',companyName, '\n', formNumber, '\n',incomeOrder, '\n',providerCode);

        // match(/.*ОРДЕР(.*)\n?Номер.*/m)



        // axios({
        // 	method: 'post',
        // 	url: `http://localhost:3003/api/users/test-tesseract-ocr`,
        // 	data: {text}
        // })
        // 	.then(() => {

        let tesserResult ={incomeOrder:incomeOrder,providerCode:providerCode,guestNumber:guestNumber,buyingOrder:buyingOrder, text:newText};

        // let companyNameREGEX = '^АО&#171&#187$';
        // let companyName = text.match(companyNameREGEX);
        // console.log(companyName);
        // let tesserResult={};
        res.json(tesserResult)
        //
        // 	})
        // 	.catch(next);

        // try {
        // 	const result = await axios({
        // 		method: 'post',
        // 		url: `http://localhost:3003/api/users/test-tesseract-ocr`,
        // 		data: {text}
        // 	})
        //
        // 	res.json()
        // }catch (ex){
        // 	next(ex)
        // }

    } catch (ex) {
        next(ex)
    }

// Recognize German text in a single uniform block of text and set the binary path

    // var options = {
    // 	// l: 'eng+rus+ukr',
    // 	l: 'ukr',
    // 	// l: 'rus',
    // 	psm: 6,
    // 	// binary: '/usr/local/bin/tesseract'
    // 	binary: '/usr/bin/tesseract'
    // };

    // tesseract.process(`${__dirname}/${fileJpg}`, options, async function (err, text) {
    // tesseract.process(fileJpg, options, async function (err, text) {
    // 	if (err) {
    // 		console.error(err);
    // 	} else {
    // 		console.log(text);
    //
    // 		// axios({
    // 		// 	method: 'post',
    // 		// 	url: `http://localhost:3003/api/users/test-tesseract-ocr`,
    // 		// 	data: {text}
    // 		// })
    // 		// 	.then(() => {
    // 		 		res.json(text)
    // 		//
    // 		// 	})
    // 		// 	.catch(next);
    //
    // 		// try {
    // 		// 	const result = await axios({
    // 		// 		method: 'post',
    // 		// 		url: `http://localhost:3003/api/users/test-tesseract-ocr`,
    // 		// 		data: {text}
    // 		// 	})
    // 		//
    // 		// 	res.json()
    // 		// }catch (ex){
    // 		// 	next(ex)
    // 		// }
    // 	}
    // });


}

module.exports.parseText = function (req, res, next) {
    let text = `» Типова фарш и 144
    АО «ДТЗК ДНЕПРОЗНЕРГО> * Затверджена наказом Мінстату.
        . України від 21.06.96 р. |? 193
        ММ
        (підприємство,організація)- Код за ЩДС:
        Ідентифікаційний код 38024583
    ПРИБУТКОВИИ ОРДЕР П! 5000921679
    документа складання операції Найменування рахунок, Код супровідного
    субрахунок аналітичного документа
    обліку
    12-09.2018
        , 768231 Постачальник К765432 Н3214234
    МЗ2355454 ”97390275634834 М976801364632946
    Порядком
    Наїшенування, сорт, номеншдщрщй Рахунок, найМенува за д0кументо Прийнято Ціна грн. Сума грн. Номер Номер
    озиір, марка / ГОСТ, ТУ, номер субрахунок. ння фактично паспорта запису за.
        чтд обліку ТМЦ складськш
        . - калтоте'кой
    шин-п.- 11
:.нолвкэ. 90009478 206001-6100 М 5 *832 00 -
    Всього без ПДВ: 5 взяло .
        Ш-
“Щ-
. ЙОЄЩЇІБМО щові'ооооо 5 832, 00
    Синтетичний рахунок для
        , відає-гоєння зн'оСу- кол по шт « .`;
// &#171 &#187
//     Кавычки «ёлочки». HTML код (мнемоника): &#171; &#187; (&laquo; &raquo;)
//     Двойные универсальные. HTML код (мнемоника): &#34; (&quot;)

    let companyNameREGEX = /[«<](.*)[>»]/;
    // let companyNameREGEX = new RegExp(`[«<](.*)[>»]`);
    let companyName = text.match(companyNameREGEX);
    console.log(companyName[1]);

    let formNumberREGEX = /№(.*)\n/i;
    let formNumber = text.match(formNumberREGEX);
    console.log(formNumber);

    // let UKUDCodeREGEX = '(?<=[УКУД].*(?=[дентиф]))';
    // let UKUDCode = text.match(UKUDCodeREGEX);
    // console.log(UKUDCode);
		//
    // let IdentificCodeREGEX = '(?<=[йний код].*(?=[ПРИБУТКОВИЙ]))';
    // let IdentificCode = text.match(IdentificCodeREGEX);
    // console.log(IdentificCode);
		//
    // let IncomeOrderREGEX = '(?<=[ОРДЕР №].*(?=[Номер]))';
    // let IncomeOrder = text.match(IncomeOrderREGEX);
    // console.log(IncomeOrder);

    // return res.json({
    //     companyName: companyName
    // });
};

function getRandom(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

module.exports.saveToBaseEditedResult = function(req,res,next){
    try{
        const { confirmedText } = req.body;
        let str='';
        for(let i =0; i<6; i++){
            str += (getRandom(0,9).toString());
            console.log(str);
        }
        Object.assign(confirmedText, {serialNumber: str});

        console.log('this is confirmed text',confirmedText);
        scanOfDocModel.create(confirmedText, function (err, text) {
            if (err) {
                console.error(err);
                return next(err);
            }
            // next()
             res.status(200);
             res.json({
                 text
             });
        });
    }catch (error) {
        console.log('this is error text',error);
    }
};

module.exports.getTheDataFromCodeNumber = function (req, res, next) {
    let {code} = req.body;
    console.log(code);
    scanOfDocModel.find({serialNumber: code}, function (err, ScanOfDoc) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        console.log(ScanOfDoc);
        return res.json({
            ScanOfDoc: ScanOfDoc
        })
    });
};

module.exports.login = function (req, res, next) {
    let {user} = req.body;
    console.log('this is server user', user);
    const arrUser = user.split(" ");
    const name = `${arrUser[0]} ${arrUser[1]} ${arrUser[2]}`;
    console.log('this is the name', name);
    const phone = arrUser[3];

    console.log('before find phone');
    console.log('before find phone|', name, '|', phone);
    userModel.find({name: name, phone: phone}, function (err, user) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        console.log(user);
        return res.json({
            user: user
        })
    });
};

module.exports.checkName = function (req, res, next) {
    let {name} = req.body;
    console.log('this is server user', name);
    userModel.find({name: name}, function (err, name) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        console.log(name);
        return res.json({
            name: name
        })
    });
};

module.exports.logVisits = function (req, res, next) {
    let {user} = req.body;
    let arrUser = user.split(" ");
    const name = `${arrUser[0]} ${arrUser[1]} ${arrUser[2]}`;
    console.log('this is the name', name);
    const visit = {};
    visit.name = name;
    visit.date = new Date();
    console.log('before create visit', visit);

    logOfVisiting.create(visit, function (err, visit) {
        if (err) {
            console.error(err);
            return next(err);
        }
        next()
        // res.status(200);
        // res.json();
    });
};

module.exports.createProduct = function (req, res, next) {
    let product = req.body;

    console.log('this is the name', product);

    productModel.create(product, function (err, product) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        res.json({product: product});
    });
};

module.exports.getProduct = function (req, res, next) {
    let product = req.params.product;
    console.log('this is the name', product);

    logOfVisiting.find({_id: product}, function (err, visit) {
        product.map()
        if (err) {
            console.error(err);
            return next(err);
        }
        // res.status(200);
        // res.json();
    });
};

module.exports.isEmail = function (req, res, next) {
    // let isEmailResult = undefined;
    let email = req.body.email;
    console.log('server', email);

    userModel.find({email: email}, function (err, isEmailResult) {
        if (err) {
            console.error(err);
            return next(err);
        }
        return res.json({
            isEmailResult: !!isEmailResult.length
        });

        // if (isEmailResult) {
        //     // console.log("server",isEmailResult);
        //     res.json({
        //         isEmailResult: true
        //     })
        // }
        // else {
        //     // console.log("serverFalse",isEmailResult);
        //     res.json({
        //         isEmailResult: false
        //     })
        // }
    })

};

module.exports.isUser = function (req, res, next) {
    if (req.isAuthenticated()) {
        // console.log(req.user);
        res.json({
            name: req.user.name,
            avatar: req.user.avatar
        });
    }
    else {
        res.statusCode = 403;
        res.end()
    }
};

module.exports.logout = function (req, res, next) {
    req.logOut();
    res.end();
};

module.exports.fetch = function (req, res, next) {
    userModel.find({}, (err, users) => {
        if (err) {
            console.error(err);
            return next(err);
        }
        setTimeout(() => res.json({users: users}), 2000);
        // setTimeout(function (){res.json({users: users})}, 2000)
    })
};

module.exports.createPushToken = async (req, res, next) => {
    try {
        const {idUser, token: {value: token} = {}} = req.body;

        if (!token) {
            throw new Error(`There is no token received.`);
        }

        const user = await userModel.findById(req.user._id).exec();

        if (!user) {
            throw new Error(`There is no user found.`)
        }

        user.pushToken = token;
        await user.save().exec();

        res.end();
    } catch (ex) {
        next(ex);
    }
};

module.exports.sendPushNotifications2 = function () {
    console.log('asdf adfhsdhsdfghdf gnvcghjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
}

module.exports.sendPushNotifications = function (req, res, next) {
    try {
        // const {message} = req.body;
        const somePushTokens = ['ExponentPushToken[Y31awCD49C-qF1LeaRucpU]'];
        console.log('this is server side 1');
        let expo = new Expo();

// Create the messages that you want to send to clents
        let messages = [];
        for (let pushToken of somePushTokens) {
            // Each push token looks like ExponentPushToken[xxxxxxxxxxxxxxxxxxxxxx]

            // Check that all your push tokens appear to be valid Expo push tokens
            if (!Expo.isExpoPushToken(pushToken)) {
                console.error(`Push token ${pushToken} is not a valid Expo push token`);
                continue;
            }

            // Construct a message (see https://docs.expo.io/versions/latest/guides/push-notifications.html)
            messages.push({
                to: pushToken,
                sound: 'default',
                body: 'This is a test notification',
                data: {withSome: 'data'},
            })
        }
        console.log('this is server side 2');
        let chunks = expo.chunkPushNotifications(messages);
        let tickets = [];
        (async () => {
            // Send the chunks to the Expo push notification service. There are
            // different strategies you could use. A simple one is to send one chunk at a
            // time, which nicely spreads the load out over time:
            for (let chunk of chunks) {
                try {
                    let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                    console.log(ticketChunk);
                    tickets.push(...ticketChunk);
                    // NOTE: If a ticket contains an error code in ticket.details.error, you
                    // must handle it appropriately. The error codes are listed in the Expo
                    // documentation:
                    // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                } catch (error) {
                    console.error(error);
                }
            }
        })();
        let receiptIds = [];
        for (let ticket of tickets) {
            // NOTE: Not all tickets have IDs; for example, tickets for notifications
            // that could not be enqueued will have error information and no receipt ID.
            if (ticket.id) {
                receiptIds.push(ticket.id);
            }
        }
        console.log('this is server side 3');
        let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);
        (async () => {
            // Like sending notifications, there are different strategies you could use
            // to retrieve batches of receipts from the Expo service.
            for (let chunk of receiptIdChunks) {
                try {
                    let receipts = await expo.getPushNotificationReceiptsAsync(chunk);
                    console.log(receipts);

                    // The receipts specify whether Apple or Google successfully received the
                    // notification and information about an error, if one occurred.
                    for (let receipt of receipts) {
                        if (receipt.status === 'ok') {
                            continue;
                        } else if (receipt.status === 'error') {
                            console.error(`There was an error sending a notification: ${receipt.message}`);
                            if (receipt.details && receipt.details.error) {
                                // The error codes are listed in the Expo documentation:
                                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                                // You must handle the errors appropriately.
                                console.error(`The error code is ${receipt.details.error}`);
                            }
                        }
                    }
                } catch (error) {
                    console.error(error);
                }
            }
        })();

    } catch (ex) {
        console.log(ex);
    }
}





